+++
title = "Security Setup"
date = 2022-03-01T08:42:08-05:00
weight = 10
pre = "<b>2. </b>"
+++



The operational goals for this project means that these computers will be exposed to the internet and I want to do my best to keep them relatively safe.
I also want to make sure that security is automated, centralized to the projects automation, and versioned.
