---
title: "Generating Keys"
weight: 20
---

### Required Keys and Tokens for Deployment

| Type | Name | Description|
| ---: | ---: | --------- |
| Token| token:k3s  | Token for registering agents with the controller nodes.|
| Token| token:github | Token for authenticating to [github.com](www.github.com) apis.|
| Token| token:gitlab  | Token for authenticating with [gitlab.com](www.gitlab.com) apis.|
| SSH  | ssh:github, ssh:github.pub | Key pair for SSH authentication of github repos.|
| SSH  | ssh:gitlab, ssh:gitlab.pub | Key pair for SSH authentication of github repos.|
| SSH  | ssh:bastion, ssh:bastion.pub| Key pair for SSH authentication to bastion node.|
| SSH  | ssh:k3s, ssh:k3s.pub    | Key pair for SSH authentication to k3s nodes.|
| GPG  | gpg, gpg.pub | Key pair for secret encryption. |

### Steps for generation :
---

1. Get a [GitHub personal access token](https://docs.github.com/en/authentication/keeping-your-account-and-data-secure/creating-a-personal-access-token).
    1. Put it in a password manager.
1. Get a [GitLab personal access token](https://gitlab.com/-/profile/personal_access_tokens).
    1. Put it in a password manager.
1. Run the `security_setup` script at `/scripts/security_setup`.
1. After the script completes check the following :
    1. SSH key uploaded to [GitHub](https://github.com).
    1. SSH key uploaded to [GitLab](https://gitlab.com).
    1. Store your SSH passphrases in a password manager.
    1. Store your gpg keys in a password manager. (This is the only way to decrypt your secrets, so make sure you keep it backed up somewhere safe.)

### Using your keys after generation :
---
1. Decrypt : `git secret reveal secrets/*`.
1. Clean up : `find secrets/ -type f ! -name '*.secret' -delete`
1. Encrypt a new secret : `git secret add <path> && git secret hide && git add <path>.secret`
    1. Run the clean up script after, don't check in anythin but the actually encrypted file.
