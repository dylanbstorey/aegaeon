---
title: "Security Layout"
weight: 10
---

### Security Diagram
---

{{< mermaid align="center" >}}

%%{initialize: { 'logLevel': 'fatal', "theme":'dark', 'startOnLoad': true } }%%

flowchart LR

    subgraph github["GitHub"]
        gh_ssh["ssh:github.pub"]
    end

    subgraph gitlab["GitLab"]
        gl_ssh["ssh:gitlab.pub"]
    end

    subgraph dev["Developer Box"]
        dev_bp["ssh:bastion"]
        dev_gh["ssh:github"]
        dev_gl["ssh:gitlab"]
        dev_gpg["gpg"]
    end

    subgraph bastion["Bastion"]
        bas_k3s["ssh:k3s"]
        bas_gh_tok["token:github"]
        bas_gh["ssh:github"]
        bas_bp["ssh:bastion.pub"]
        bas_gl["ssh:gitlab"]
        bas_gl_tok["token:gitlab"]

    end

    subgraph k3s["k3s Nodes"]
        k3_ssh["ssh:k3s.pub"]



    end

    dev -.- bastion -.- k3s


    dev_gh ==> gh_ssh
    bas_gh ==> gh_ssh

    dev_gl ==> gl_ssh
    bas_gl ==> gl_ssh


    dev_bp == "Password" ==> bas_bp
    bas_k3s == "Password" ==> k3_ssh

    bas_gl_tok --> gitlab
    bas_gh_tok --> github


    dev_gpg -.-> dev_gpg

{{< /mermaid >}}
