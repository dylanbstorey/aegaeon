---
title: Hardware
weight: 10
---

{{% notice tip %}}
Just a note - A fair amount of this equipment is stuff I had lying around from other projects, i didn't (and don't suggest) going out and
just buying all of this stuff for a weekend project like this.
{{% /notice %}}



### Initial List for `k8s` cluster


- 8 x [Rasberry Pi 4](https://www.amazon.com/dp/B0899VXM8F?psc=1&ref=ppx_yo2_dt_b_product_details)
- 8 x [Ethernet Cables](https://www.amazon.com/dp/B01CRFVIWO?psc=1&ref=ppx_yo2_dt_b_product_details)
- 2 x [microSD Cards](https://www.amazon.com/dp/B077QSGMCN?psc=1&ref=ppx_yo2_dt_b_product_details)
- 8 x [Power Over Ethernet (POE) Hats ](https://www.amazon.com/dp/B092VVFKWT?psc=1&ref=ppx_yo2_dt_b_product_details)
- 1 x [Ethernet Switch (with POE)](https://www.amazon.com/dp/B082KM1796?psc=1&ref=ppx_yo2_dt_b_product_details)
- 1 x [Rasberry Pi Case](https://www.amazon.com/dp/B089QY975B?psc=1&ref=ppx_yo2_dt_b_product_details)
- 8 x [Samsung 980 SSD](https://www.amazon.com/dp/B08V7GT6F3?psc=1&ref=ppx_yo2_dt_b_product_details)
- 8 x [NVMe to USB 3.1 Adapter](https://www.amazon.com/dp/B07VVNGYFV?psc=1&ref=ppx_yo2_dt_b_product_details)





### NAS storage

- 1 x [Synology 8 Bay Disk Station](https://www.amazon.com/dp/B08PKLKKVZ/ref=cm_sw_r_tw_dp_KR7EYWNAS889V58QF9PG?_encoding=UTF8&psc=1)
- 4 x [ WD Red 4 TB Disks](https://www.amazon.com/gp/product/B00EHBERSE/ref=ppx_yo_dt_b_search_asin_title?ie=UTF8&th=1)
- 4 x [ WD Red 3 TB Disks](https://www.amazon.com/gp/product/B008JJLW4M/ref=ppx_yo_dt_b_search_asin_title?ie=UTF8&psc=1)
