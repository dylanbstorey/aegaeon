---
title: External Requirements
weight: 10
---

### External Accounts

1. [GitHub](www.github.com) - version control for GitHub based projects.
    1. [GitHub Token](https://docs.github.com/en/authentication/keeping-your-account-and-data-secure/creating-a-personal-access-token) - - Generate an admin token for GitHab, put it into a password manager.
1. [GitLab](www.gitlab.com) - version control for GitLab based projects.
    1. [GitLab Token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) - Generate an admin token for GitLab, put it into a password manager.
1. [NoIp.com](www.noip.com) - Dynamic DNS system, get an account and a DNS address reserved.


### Tools

1. [Git Secret](https://git-secret.io/) - secret encryption in your git repo.
1. [Cloud Init](https://cloudinit.readthedocs.io/en/latest/) - incrastructure initialization
