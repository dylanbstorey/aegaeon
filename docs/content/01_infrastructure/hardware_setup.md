---
title: Hardware Setup
weight: 30
---





## NAS Appliance Setup

1. Install hard drives into the station.
1. Plug the device into the network.
1. Do initial OS account setup.
1. Setup storage volumes.
1. Enable DDNS support to [NoIP.com](https://my.noip.com).
1. *__Optional__ : Forward port 5000 on your router to the NAS on the router if you want to access storage from outside your home network.*
1. Enable SSH access to your NAS appliance, login and run the following command, `id`. Record the UID and GID so that any deployment that mounts it as a PVC can have the correct PUID, PGID set on it.



## Rasberry Pi Setup

1. Install the rasberry pi modules onto the case holders.
1. Install your fans onto the case. (I had to modify the fans to run from USB because the POE hat takes up the power pins.)
1. Make sure the computers, network switch, and cabling fit inside of your case like you'd like.
1. Remove everything from the case for the kernel upgrade process.

#### Upgrade Kernel
{{% notice note %}}
This may not be strictly necessary to update the EEPROM image on newer rasberry pi's in order to enable USB boot, but its kind of a pain
{{% /notice %}}


1. Go get the [Rasberry Pi Imager](https://www.raspberrypi.com/software/) and flash a microSD Card using EEPROM image available under `Misc utility images`.
1. Insert the microSD into your Rasberry Pi and wait 2 minutes for the update to occur. (The green light next to the power indicator will flash periodically when complete, it shouldn't take more than 30 seconds but easier to just wait excessive time.)
1. Power down the Pi, remove the microSD card, and install the Pi into its case.
