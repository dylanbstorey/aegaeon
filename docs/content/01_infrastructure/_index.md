+++
title = "Basic Infrastructure"
date = 2022-01-11T13:00:33-05:00
weight = 5
pre = "<b>1. </b>"
+++

A shopping list and some initial steps taken to get hardware and external accounts ready for installation.
