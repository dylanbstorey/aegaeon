+++
title = "k3s"
date = 2022-03-02T14:50:16-05:00
weight = 30
pre = "<b>3. </b>"
+++


This section covers taking us from hardware to a functional k3s cluster and initial services setup.

The basic proceedure for installing this software is :
- generate files for the cluster
- flash your USB drive with `Ubuntu 21.10` using [Rasberry Pi Imager]().
- copy the appropriate `user-data` file to the USB drive
- plug the USB drive into your rasberry pi
- power up the rasberry pi
    - wait while the computer bootstraps itself.


{{% notice note %}}
The only file that needs to be modified is the `cluster_config`, there is a file `scripts/fixtures` that helps to manage all of the environment variables when the script needs them. If there are any problems with your generated files (missing spots), check there first.
{{% /notice %}}
