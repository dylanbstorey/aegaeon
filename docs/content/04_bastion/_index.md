+++
title = "Bastion"
date = 2022-01-11T13:00:33-05:00
weight = 40
pre = "<b>4. </b>"
+++

The bastion node is serving as our buffer so theres a secure location to login to from outside the network and do work. We'll be using it for some other functions in the future as well that are
for within the LAN when we go with an HA control plane in the next iteration.

It'll also have some executables pre packaged in for cluster management :
- [Flux2](https://fluxcd.io/docs/) - git ops for managing cluster state
- [Kubeseal](https://sealed-secrets-operator-helm.readthedocs.io/en/latest/) - secret encryption for kubernetes services
- [k9s](https://k9scli.io/) - graphic CLI interface for managing k8s clusters
- [kubectl](https://kubernetes.io/) - major CLI for interacting with your k8s cluster directly
