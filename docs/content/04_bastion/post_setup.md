---
title: Bastion as Proxy
weight: 20
---
You can do this after you've got some applications up and running, I could have built it into the cloud-init, but it didn't seem necessary as I should only do this once.

In order to serve web applications outside of the home network, we need to put a proxy to manage routing of message from the internet to the k8s cluster.

We just use `nginx` for this, its simple and straight forward to setup.


1. Log in to the `bastion` node.
1. `sudo nano /etc/nginx/sites-enabled/default`
1. Add the following location line into the server block
```bash
 server {

     ...
     location / {
        rewrite ^/(.*) /$1 break;
        proxy_pass http://10.0.0.200:80;
        }
    ...
 }
```
1. `sudo nginx -s reload` to restart the nginx server
1. verify you can reach an application on your k8s cluster by using the bastion ip + set prefix. (i.e. `10.0.0.15/alive`)

## Security

Now that you're opening up to the external internet, you should be thinking about some security as well. This configuration on the bastion node
will put a single point of entry for login.

1. `sudo apt-get install apache2-utils`
1. `sudo htpasswd -c /etc/apache2/.htpasswd <user_name>`, you'll be asked to type a password in.
1. `sudo nano /etc/nginx/sites-enabled/default`.
1. Add the following lines to the server block.

```bash
   ...
server{
    auth_basic      "No Peeking";
    auth_basic_user_file /etc/apache2/.htpasswd;
    ...
    }
```
1. `sudo nginx -s reload` to restart the nginx server
1. Navigate to an application, you should get a login prompt.
