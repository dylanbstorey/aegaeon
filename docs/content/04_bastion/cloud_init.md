---
title: Bastion Install
weight: 10
---

{{% notice warning %}}
This computer can come up at any time - however, the kube config files won't be available until you copy the config file over.
{{% /notice %}}

### tl;dr


1. Run `./scripts/generate_bastion_node`.
    1. This will generate a `user-data` file in `generated_assets/bastion`.
1. Manually inspect the user-data to ensure no blank spots.
1. Flash `Ubuntu 20.10` onto your USB drive.
1. Copy the data in the `user-data` file on the flash drive.
1. Boot it and walk away

#### Manual Steps

1. copy the kubeconfig file from the primary node
1. set the KUBECONFIG variable in your bash_profile
1. source it

```
mkdir ~/.kube
scp aegaeon-c001:/etc/rancher/k3s/k3s.yaml ~/.kube/conf
echo "export KUBECONFIG=~/.kube/config" >> .bashrc
source ~/.bashrc
```


#### Validation

1. run `flux check --pre`
1. run `helm --version`
1. run `kubeseal --version`
1. run `k9s`
