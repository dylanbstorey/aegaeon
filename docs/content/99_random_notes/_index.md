+++
title = "Random Notes"
date = 2022-03-01T08:42:08-05:00
weight = 1000
+++


# Adding the bastion to the cluster:

After a long time it was pretty clear to me that the bastion node could probably also serve as an agent node to provide expanded capacity. So I added it to the cluster as well.

# Kernel Upgrades:

Because the cluster is small i'll occasionally log into the bastion node and do some basic maintainence (package upgrades etc.). Be careful with kernel upgrades, they pretty regularly cause
issues with longhorn for some reasons.

Run `uname -a` before you do anything and jot down the current functioning kernel version.

In the event something goes wrong post restart :

- `dpkg –list|grep linux-image` get the new image name
- `dpkg –list|grep linux-headers` get the new headers name
- run ` sudo apt-get purge <image_name> -y && sudo apt-get purge <header_name> -y && sudo reboot now`
