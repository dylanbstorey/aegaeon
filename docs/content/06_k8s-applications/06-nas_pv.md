---
title: "NAS Storage"
weight: 60
pre: "6. "
---

> nfs-common was needed to get this running correctly, i've added it to the base cloud-init file

### NAS server setup
1. Setup a shared folder on your NAS server.
    1. Create a shared folder
    1. Click edit and navigate to NFS Permissions set the following :
        * Hostname of IP: *
        * Privilege: Read Write
        * Squash: No Mapping
        * Security: sys
        * Enable asynchronous
        * Enable Allow users to access mounted subfolder
1. Grab the name of the mount path while you're here. (`/volume2/media`)

### Persistance Volume Object

Create as "many as you need"

```yaml
apiVersion: v1
kind: PersistentVolume
metadata:
  name: media-storage  # < Name of the persistent volume
  namespace: default
spec:
  storageClassName: ""
  capacity:
    storage: 5Ti # < Maximum storage size you want to reserve
  accessModes:
    - ReadWriteOnce
  persistentVolumeReclaimPolicy: Retain
  mountOptions:
    - hard
    - nfsvers=3
  nfs:
    server: 10.0.0.10  # < The ip adress of your NAS (NFS Server)
    path: "/volume2/media"  # < The NFS volumename
    readOnly: false
```

### Persistent Volume Claim

```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: alive-nas-pvc
  namespace: alive
spec:
  storageClassName: ""
  volumeName: alive-pv # < The volumename needs correpond with the persistent volume
  accessModes:
   - ReadWriteMany
  resources:
    requests:
      storage: 1Mi
```
