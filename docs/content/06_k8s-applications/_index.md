+++
title = "Deployments"
date = 2022-03-02T14:50:16-05:00
weight = 60
pre = "<b>6. </b>"
+++

This is basically a list of everything we're deploying into the cluster and an explanation of what it is and what it does for us. I expect this to get bigger over time.

The instructions here are all the minimum required to do the initial deployment, however after initial deployments I decided to break the large multi document yaml files out
into individual k8s objects. It's more files, but also gives me some more modularity for re-use. The program [kubectl-split](https://golangexample.com/split-multiple-kubernetes-files-into-smaller-files-with-ease-2/) was used.
Before you use blindly, I ran into scenarios where the last line of the input file was truncated, check that before you blindly commit.

You can watch any flux reconcilliation with the following command : `watch -n1 flux get kustomization
