---
title: "MetalLB"
weight: 30
pre: "3. "
---

Since kubernetes doesn't ship with load balance we need to install one. [MetalLB](https://metallb.universe.tf/), is pretty much the standard here. If you're running on a cloud provider, you're safe to ignore this entirely.


1. `cd aegaeon-cluster && mkdir metal-lb`
1. Create our secret file :
```
apiVersion: v1
data:
  secret: <secret-value>
kind: Secret
metadata:
  creationTimestamp: null
  name: memberlist
  namespace: metallb-system
```
1. run kubeseal to encrypt the secret, set scope to strict, and check it in.
1. `wget https://raw.githubusercontent.com/metallb/metallb/v0.9.3/manifests/namespace.yaml`
1. `wget https://raw.githubusercontent.com/metallb/metallb/v0.9.3/manifests/metallb.yaml`
1. create your config map :
```
kind: ConfigMap
apiVersion: v1
metadata:
  name: config
    namespace: metallb-system
data:
  config: |
    address-pools:
    - name: default
      protocol: layer2
      addresses:
      - 10.0.0.200-10.0.0.254 # This is the IPs from your internal network that can be used, I have a big pool (100-254) and am using a later range. This will wind up being the extrnal IPs that are presented.
```
1. set your kustomize and let it go
1. run `kubectl get svc -A` and see that some sort of External IP is created on the  LoadBalancer resource.
