---
title: "VPA: Vertical Pod Autoscaler"
weight: 90
pre: "9. "
---


The Vertical Pod Autoscaler (VPA) in Kubernetes optimizes resource allocation for pods. It automatically adjusts CPU and memory based on usage, improving efficiency. VPA minimizes manual tuning, ensuring pods have resources they need, reflecting modern, automated cloud management.

Install via flux to make life easier, we've opted for a helm chart at https://cowboysysop.github.io/charts/.

To use add a VPA object to your manifest :

```
---
apiVersion: autoscaling.k8s.io/v1
kind: VerticalPodAutoscaler
metadata:
  name: transmission-vpa
  namespace: media-scrapers
spec:
  targetRef:
    apiVersion: apps/v1
    kind: Deployment
    name: transmission
  resourcePolicy:
    containerPolicies:
      - containerName: '*'
        minAllowed:
          cpu: 500m
          memory: 100Mi
        maxAllowed:
          cpu: 2000m
          memory: 4000Mi
        controlledResources: [cpu, memory]

```

{{% notice warning %}}
Versions of kubernetes prior to `1.27`, the VPA was of limited usage because it required a restart (and two replicas) to take effect. Make sure you're running at least 1.27, and probably a higher version to make full use of this feature.
{{% /notice %}}
