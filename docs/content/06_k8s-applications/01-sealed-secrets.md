---
title: "Bitnami: Sealed Secrets"
weight: 10
pre: "1. "
---

Sealed secrets allow us to put secrets directly into our github repository and manage deployments
with flux.

The kubeseal binary is already installed as part of the bastion `cloud-init` file. It looks like the
easiest way to use this is to run sealing steps on the bastion node where kubeseal has access the cluster kube-config.

#### Controller Installation

This installs the cluster side application that unseals secrets.

1. `mkidr aegaeon-cluster/sealed-secrets`
1. `wget https://github.com/bitnami-labs/sealed-secrets/releases/download/v0.17.3/controller.yaml`
1. setup a `kustomization.yaml`
1. check deployment with : `kubectl get po -n kube-system`

#### Usage

1. Create your secret as a yaml.
1. Update it with correct / raw information.
1. Run `kubeseal --scope cluster-wide -o yaml < secret.yaml > sealed-secret.yaml`

These are the possible scopes:

- strict (default): the secret must be sealed with exactly the same name and namespace. These attributes become part of the encrypted data and thus changing name and/or namespace would lead to "decryption error".
- namespace-wide: you can freely rename the sealed secret within a given namespace.
- cluster-wide: the secret can be unsealed in any namespace and can be given any name.

You can check the *SEALED SECRET* into the repo directly.
