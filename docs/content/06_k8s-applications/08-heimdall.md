+++
title = "Homepage"
weight = 80
pre = "8. "
+++


[Homepage](https://gethomepage.dev/) is a static application dashboard I'm using as a landing page for my internal network.


The documentation for the application is extensive and pretty well thought out - just RTFM and use your `config-map.yaml` to configure your application settings.
