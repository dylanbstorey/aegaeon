---
title: "Postgres"
date: 2022-05-19T01:24:07-04:00
description: Database to back serveARR applications
---


We're planning on using PostGres to maintain our application state on all the *arr stack.

This deployment is fairly vanilla, check the `config-map` and `sealed-postgres-secret` for required information to ensure that the container comes up with appropriate credentials and configuration.
