---
title: "servARR stack*"
date: 2022-05-19T01:24:15-04:00
description: "Media applications from the servARR stack."
---

## Services:
- lidarr, music
- readarr, digital and audio books
- sonarr, television
- radarr, movies
- prowlarr, index aggregator
- flareresolverr, proxy for index aggregator

---
## Init Container

We utilize an init container in the deployment to create a database for our applications if it doesn't already exist. The initialization script consists of three stage: wait until the postgres database is accepting conditions, attempt to make a select statement against the database, create the database if the select statement fails.

```
 initContainers:
        - name: init-database
          image: postgres:14
          envFrom:
            - secretRef:
                name: postgres-creds
                optional: false
          command:
            - bash
            - -c
            - |
              export PGPASSWORD=$( tr -d '\n\t\r' <<< $POSTGRES_PASSWORD )
              until pg_isready -h postgres -p 5432 -U ${POSTGRES_USER}; do
                echo "Waiting for PostgreSQL to be ready..."
                sleep 2
              done
              psql -h postgres -U ${POSTGRES_USER} -d 'radarr-main' -c "SELECT 1" || psql -h postgres -U ${POSTGRES_USER} -c 'CREATE DATABASE "radarr-main";'
              psql -h postgres -U ${POSTGRES_USER} -d 'radarr-log' -c "SELECT 1" || psql -h postgres -U ${POSTGRES_USER} -c 'CREATE DATABASE "radarr-log";'
```



---
### Sealed Secret Generation/Mounting
1. Create your `config.xml`
1. `base64 -w 0 config.xml`, copy output.
1. Insert that output into your Secret:
```
apiVersion: v1
data:
  config.xml: pen...15
kind: Secret
metadata:
  name: lidarr-creds
  namespace: media-scrapers
type: Opaque
```
1. Seal `kubeseal --scope strict -o yaml <config-secret.yaml > ../sealed-config-secret.yaml`.
1. Define as volume in deployment:
```
volumes:
    - name: lidarr-config-xml
        secret:
            secretName: lidarr-creds
```
1. Mount it:
```
mountPath: /config/config.xml
    subPath: config.xml
    name: lidarr-config-xml
    readOnly: True
```
