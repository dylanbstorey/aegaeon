---
title : "Transmission"
weight : 10
description : "A torrent client that runs over an Open VPN client."
weight : 30
---

My first time doing this install I tried to put MOST of it on the longhorn PVF and have the completed files go to the NAS drive. The heavy IO started causing some serious issues with transfer interupts, so I'd recommend not doing that.

Getting this configured was also a pain in the ass, i had to run it in just a docker container multiple times to get it configured properly and functioning.

### Environment Variables

```bash
  TRANSMISSION_HOME="/longhorn_storage/transmission"
  TRANSMISSION_INCOMPLETE_DIR="/nas_storage/transmission/incomplete"
  TRANSMISSION_DOWNLOAD_DIR="/nas_storage/transmission/complete"
  TRANSMISSION_WATCH_DIR="/nas_storage/transmission/watch"
```
