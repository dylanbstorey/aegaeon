---
title: "nginx Ingress Controller"
weight: 20
pre : "2. "
---

The ingress controller is very descriptively named, it controls ingress to the cluster and manages routing to internal services via a single exposed IP address. (This is a gross simplification but it's good enough for now.)

1. `cd aegaeon-cluster && mkdir nginx-ingress-controller`
1. `wget https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v1.0.4/deploy/static/provider/aws/deploy.yaml`
1. make your kustomize file and deploy.
1. check deployment with `kubectl --namespace default get services -o wide -n ingress-nginx`
