---
title: "Alive"
weight: 40
pre: "4. "
---

Just a very simple web service to demonstrate routing and that the cluster is in fact working.


To use:

1. get the ExternalIP for your load balancer (via `kubectl get svc -A`)
1. open a web browser to `<EXTERNAL-IP>/alive`
