---
title: "LongHorn"
weight: 50
pre: "5. "
---

Longhorn is a distributed file system for kubernetes developed and release by Rancher. It's good local storage for compute functions.

This deployment required the use of a HelmChart and Release object for flux. The ingress looks a little weird but [this issue](https://github.com/longhorn/longhorn/issues/1745) explains why.

The only manual step is to create a sealed secret for the password authentication on the ingress controller.

### Create Sealed Secret

```bash
USER=<user-name>
PASSWORD=<password>
echo "${USER}:$(openssl passwd -stdin -apr1 <<< ${PASSWORD})" >> auth

kubectl -n longhorn-system create secret generic basic-auth --from-file=auth --dry-run=client -o yaml
> secret-basic-auth.yaml

kubeseal --scope strict -o yaml < secret-basic-auth.yaml > sealed-secret-basic-auth.yaml
```

## Test Installation
---

### Create a PVC

1. Create a file pvc.yaml
```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: test-pvc
spec:
  accessModes:
  - ReadWriteOnce
  storageClassName: longhorn
  resources:
    requests:
      storage: 1Gi
```
2. apply it with `kubectl apply -f pvc.yaml`
3. watch the longhorn ui, you should see it appear and then "detatch"

### Mount within a Pod
1. create a pod definition `test-mount.yaml`

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: volume-test
spec:
  containers:
  - name: volume-test
    image: nginx:stable-alpine
    imagePullPolicy: IfNotPresent
    volumeMounts:
    - name: test-pvc
      mountPath: /data
    ports:
    - containerPort: 80
  volumes:
  - name: test-pvc
    persistentVolumeClaim:
      claimName: test-pvc
```
2. apply with `kubectl apply -f test-mount.yaml`
3. watch the long horn ui - you'll see the PVC mount to this service and become active in the UI
