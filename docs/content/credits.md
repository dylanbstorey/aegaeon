---
Title : "Credits"
---


Websites and resources that have helped me along my way.


- https://blog.codybunch.com/2020/10/13/Kubernetes-Fix-Node-Password-Rejected/
- rpi4cluster.com
- https://blog.quickbird.uk/domesticating-kubernetes-d49c178ebc41
- https://www.devtech101.com/2019/02/23/using-metallb-and-traefik-load-balancing-for-your-bare-metal-kubernetes-cluster-part-1/
- https://platform9.com/blog/understanding-kubernetes-loadbalancer-vs-nodeport-vs-ingress/
- https://medium.com/google-cloud/kubernetes-nodeport-vs-loadbalancer-vs-ingress-when-should-i-use-what-922f010849e0
- https://anthonynsimon.com/blog/kubernetes-cluster-raspberry-pi/
- https://blog.thenets.org/how-to-create-a-k3s-cluster-with-nginx-ingress-controller/
- https://blog.sighup.io/sealed-secrets-in-gitops/
- https://www.debontonline.com/2020/10/part-10-how-to-configure-persistent.html
- https://www.jericdy.com/blog/installing-k3s-with-longhorn-and-usb-storage-on-raspberry-pi
- https://medium.com/geekculture/bare-metal-kubernetes-with-metallb-haproxy-longhorn-and-prometheus-370ccfffeba9
