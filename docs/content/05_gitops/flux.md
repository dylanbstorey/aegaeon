---
title: Flux
weight: 10
---

Flux is the system we're going to use for git ops. It's already been installed on the bastion node.

### Flux Install

1. Login to the bastion node.
1. Verify flux is ready to go with `flux check --pre && flux check`
1. Run the following :

```bash
export GITLAB_TOKEN=`cat ~/.gitlab.token`
 flux bootstrap gitlab --owner=dylanbstorey --repository=aegaeon --branch=main --path=aegaeon-cluster --personal
```

### Verify Installation
1. check your kubernetes cluster - make sure that the `flux-system` has been deployed
1. check your git repo - make sure that files were checked in to `flux/flux-system`
