+++
title = "GitOps"
date = 2022-01-11T13:00:33-05:00
weight = 50
pre = "<b>5. </b>"
+++

I've decided to take a GitOps approach for one major reason - it allows me to keep the state of my cluster stored external to the cluster so that it can be
recreated at any time. The basic idea here is; instead of running `kubectl` commands directly against the cluster, we check the applications into an external
repo, and an application on the cluster reads that state and applies it to the cluster. The major reason for this is recovery is really simple if we need to recover from scratch.

Now this doesn't mean that application state is stored but at least we know the configuration of all of our controllers and applications - which is a plus.
