---
title: "Aegaeon"
---

# Aegaeon Home Lab
---


This website documents: my home computing lab, how its built, and how I interact with it. This is meant to allow me to document my journey as I get it set up and hopefully make doing this a second time easier if I need to. (Or want to extend it over time.)

This site and all associated files are available on [gitlab](https://gitlab.com/dylanbstorey/aegaeon).


### General Operational Goals

- git-ops for kubernetes cluster definition
- deployed services are externally reachable from the internet
- be able to add nodes to the cluster with minimal work
- storage capacity on cluster sufficient to medium data loads (less than a TB)
- storage capacity off cluster for all of my media

### Use cases

- gathering and organizing media for myself to enjoy.
- development playground to play with new technology stacks / techniques

### Planned Layout

The current layout for this project is depicted below. It currently is a:
- A bastion node to serve as a single point of entry to the cluster
- A k8s node (using k3s):
    - 1 node for the control plane
    - 6 nodes for agents
    - 1 NAS system for storage

Over time (and as the great Pi shortage of 2021+ hopefully subsides) I plan on adding a second set of compute nodes for exapansion by :

- 2 rasberry pis to make an HA control plane
- 6 rasberry pis for additional agents (or maybe other stand alone applications like databases)

![Architecture plan](/aegaeon_diagram.svg)
