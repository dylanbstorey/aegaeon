# aegaeon

HomeOps for the development lab.

Documentation available at https://dylanbstorey.gitlab.io/aegaeon/.


## General flow

1. Gather starting info:
    - [GitLab Token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html)
    - [GitHub Token](https://docs.github.com/en/authentication/keeping-your-account-and-data-secure/creating-a-personal-access-token)
    - install gitsecrets + gpg
1. Do initial security bootstrap
    - creates gpg keys
    - creates multiple ssh keys
    - creates SSH keypairs for bastion node
    - creates SSH keypairs for the k3s nodes
    - creates Token for k3s nodes
    - creates + upload service keys for gitlab/github
