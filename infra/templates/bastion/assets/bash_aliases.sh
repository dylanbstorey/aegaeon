#!/bin/bash
#
# General aliases

# Notes on setting up ssh aliases with specific hosts
#alias ssh-hostname="ssh user@hostname -i ~/.ssh/id_rsa"
#alias scp-hostname="scp -i ~/.ssh/id_rsa"

alias c="clear"
alias m="make"


#lazy load some aliases when you're not sudo
if [ $UID -ne 0 ]; then
    alias reboot='sudo reboot'
    alias iotop='sudo iotop -a'
fi


alias wget='wget -c'
alias cd..='cd ..'
alias ..='cd ..'
alias ....='cd ../../'
alias df='df -H'
alias du='du -ch'
alias unique='sort -u'
